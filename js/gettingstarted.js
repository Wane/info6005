
window.onload = init;

var Gposition = null;
var lon = 0, lat = 0;				// user's current coordinate
var mapHolder = document.getElementById("mapHolder");
var map;					// map object

var markersArray = [];
var infoboxArray =[];
var OriDesMarkerArray = []	// stores origin and destination markers

// variables for destination positioning
var num = 0;				// id number for texts in infoboxs
var lonArray = [];
var latArray = [];
var titleArray = [];

// variables for origin positioning
var num_o = 0;				// id number for texts in infoboxs
var lonArray_o = [];
var latArray_o = [];
var titleArray_o = [];
var OriginOrDestinationFlag = 0;	// 0: weAreHere, 1: Origin, 2: Destination

// common names and coordinates of origin and destination 
var Origin = [];
var Destination = [];

// polyline (for plotting route)
var polylineArray = [];
var isTrackingFlag = 0;

// indicates how many times a certain function has run
var showPositionCount = 0;

// statistics
var distance = 0;
var fuelSaved = 0;
var moneySaved = 0;
var CO2Reduced =0;



/************ LocalStorage initialisation ************/

function init() {
	var sFlag = 0;
	var aFlag = 0;

	for (var i = 0; i < localStorage.length; i++) {
		if (localStorage.key(i).substring(0, 7) === "ILPT_s_") {
			sFlag = 1;		// localStroage for settings already exist
			break;
		}
		if (localStorage.key(i).substring(0, 7) === "ILPT_a_") {
			aFlag = 1;		// localStroage for achievement already exist
			break;
		}
	}

	if (!sFlag) {
		localStorage.setItem("ILPT_s_fuelType", "petrol");	
		localStorage.setItem("ILPT_s_fuelConsumption", "28.8");
		localStorage.setItem("ILPT_s_preference", "food");
		sFlag = 1;
	}
	if (!aFlag) {
		localStorage.setItem("ILPT_a_totalDistance", "0.00");	
		localStorage.setItem("ILPT_a_totalFuelSaved", "0.00");
		localStorage.setItem("ILPT_a_totalCO2Reduced", "0.00");
		localStorage.setItem("ILPT_a_totalMoneySaved", "0.00");
		aFlag = 1;
	}

	getLocation();
}


/************ Map ************/

function getLocation() {
  	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition, showError, {maximumAge: 60000});   
  	} else {
		mapHolder.innerHTML = "Geolocation is not supported by this browser.";
	}
} 

function showPosition(position) {
	var lat_last = lat;		// record the coordinates of last position (for calculating distance)
	var lon_last = lon;

  	lat = position.coords.latitude;
  	lon = position.coords.longitude;				// 200m longitude/latitude

	latlon = new google.maps.LatLng(lat, lon)
  	latlon2 = new google.maps.LatLng(lat + 0.0018, lon)	
	  
  	mapHolder = document.getElementById("mapHolder");   
  	mapHolder.style.height = "100%";
  	mapHolder.style.width = "100%";
 		
  	var center = isTrackingFlag>0 ? latlon2 : latlon;	// if is tracking, center the map a little lower to make room for recordingBar 

  	var myOptions = {           
		center: center, 
		zoom: 15,
		mapTypeId: google.maps.MapTypeId.ROADMAP,   //ROADMAP or SATELLITE or HYBRID 
		mapTypeControl: false,
		navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL}
  	};
  	map = new google.maps.Map(document.getElementById("mapHolder"), myOptions);   // generate the map
  	
  	var icon = "images/me.png";
  	var marker = addMarker(latlon, icon);		// add marker "Me", this marker isn't pushed into markerArray or be deleted
	
	var html = 'We are here now';
	OriginOrDestinationFlag = 0;
	addInfobox(marker, html);
	
	if (!showPositionCount) {		// plot detective circle and origin bus stops if it's the frist time run showPosition()
		busStopDetection(latlon);
	} else {						
		// re-plot origin and destination flags 
		replotOriAndDesFlags();

		// plot polyline to record user's route
		if ((Math.abs(lon-Origin[1]) <= 0.002) && (Math.abs(lat-Origin[2]) <= 0.002)) { 	// should be 0.0005
			polylineArray.push(latlon);

			var userRoute = new google.maps.Polyline({
    			path: polylineArray,
    			strokeColor: "#FF0000",
    			strokeOpacity: 1.0,
    			strokeWeight: 8
  			});

  			userRoute.setMap(map);
		}

		// calculate distance
		var currentPositionArray = [lon, lat];	
		var lastPosistionArray = [lon_last, lat_last];
		distance = computeDistance(lastPosistionArray, currentPositionArray);	// distance in mile
		console.log("distance: "+distance);

		// calculate saved fuel
		fuelSaved = (distance / (parseFloat(localStorage.getItem("ILPT_s_fuelConsumption")))).toFixed(2);	// fuel in imperial gallon
		
		// calculate saved money
		/* 
			此处读取并解析油价文件 
			moneySaved = fuelSaved * ;
		*/

		// calculate reduced CO2 emission
		if (localStorage.getItem("ILPT_s_fuelType") === "petrol") {
			CO2Reduced = (fuelSaved * 10.6).toFixed(2);				// CO2 emission in kg
		} else {
			CO2Reduced = (fuelSaved * 11.9).toFixed(2);				
		}

		// remove statusBar
		if (showPositionCount === 1) {								// remove the statusBar at the second run
			var statusBar = document.getElementById("statusBar");
			var div = statusBar.parentNode;
			div.removeChild(statusBar);
		}

		// display recordingBar
		recordingBar(showPositionCount);
	}
	showPositionCount++;	
}

function busStopDetection(latlon) {
  	r = 0.00209; 
	var circle = new google.maps.Circle({	// draw the detective circle
	  	center: latlon,
	  	clickable: false,
	  	radius: 200,
	  	map: map,
	  	strokeColor: "orange",
	  	strokeWidth: 2,
	  	strokeOpacity: 0.3,
	  	fillColor: "orange",
	  	fillOpacity: 0.2
	});  

	lonArray_o.length = 0;
	latArray_o.length = 0;
	deleteOverlays();

	d3.csv("data/busStops(soton).csv", function(error, data) {
		var dataset = data;
		OriginOrDestinationFlag = 1;

		for (var i = 0; i < dataset.length; i++) {
		  	if ( Math.pow(dataset[i].Longitude-lon, 2) + Math.pow(dataset[i].Latitude-lat, 2) < r*r ) {		// (x-a)^2 + (y-b)^2 < r^2 -- if a set of coordinate is within the circle
		  		lonArray_o.push(parseFloat(dataset[i].Longitude));
	  			latArray_o.push(parseFloat(dataset[i].Latitude));
	  			titleArray_o.push(dataset[i].CommonName);
		  	}
		}

		for (var i = 0; i < lonArray_o.length; i++) {
	  		latlon_s = new google.maps.LatLng(latArray_o[i], lonArray_o[i])

	  		var marker_s = addMarker(latlon_s, "images/bus2.png");
	  		markersArray.push(marker_s);
	  		addInfobox(marker_s, titleArray_o[i]); 
	  	}
	});
}

function showError(error) {
  	var errorTypes = {
		0: "Unknown error",
		1: "Permission denied",
		2: "Position is not available",
		3: "Request timeout"
  	};

  	var errorMessage = errorTypes[error.code];
  	if (error.code == 0 || error.code == 2) {
		errorMessage = errorMessage + " " + error.message;
  	}
  	mapHolder.innerHTML = errorMessage;
}


/************ functional modules ************/

function addMarker(latlon, icon) {
	var marker = new google.maps.Marker({
		position:latlon, 
	//	animation: google.maps.Animation.DROP,
		map:map, 
		clickable: true,
		icon: icon
	});
	return marker;
}

function addInfobox(marker, html) {
	var infoboxSize = 0;
	resetInfobox();

    var boxText = document.createElement("div");
	boxText.style.cssText = "border: 1px solid #AAAAAA;" +
							"margin-top: 3px;" +
							"color: #444444;" +
							"background: white;" +
							"padding: 5px;" +
							"border-radius: 7px;" +
							"box-shadow: #AAAAAA 0px 0px 5px;";

	switch (OriginOrDestinationFlag) {
		case 0 : 
			boxText.innerHTML = html;
			infoboxSize = -(html.length*8+80)/2;
			break;

		case 1 :
			boxText.innerHTML = 'From <a id="marker' + 
								num_o + 
								'" class="Origin" onclick="setOrigin(this)"><i>' + 
								html + 
								'</i></a>';
			num_o++;
			infoboxSize = -(html.length*8+110)/2;
			break;

		case 2:
			boxText.innerHTML = '<a id="marker' + 
								num + 
								'" class="Destination" onclick="setDestination(this)"><i>' + 
								html + 
								'</i></a>';
			num++;
			infoboxSize = -(html.length*8+90)/2;
			break;
	}

	var myOptions = {
		content: boxText,
		disableAutoPan: false,
		maxWidth: "400px",
		zIndex: null,
		pixelOffset: new google.maps.Size(infoboxSize, 0),	
		boxStyle: {
			minWidth: -infoboxSize + "px"
		},
		closeBoxMargin: "12px 8px 2px 10px",
		closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
		infoBoxClearance: new google.maps.Size(1, 1),
		isHidden: false,
		pane: "floatPane",
		enableEventPropagation: false
	};
	
	google.maps.event.addListener(marker, "click", function (e) {
		if (infoboxArray.length !== 0) {
			infoboxArray.pop().close();		// pop deletes and returns the last element in infoboxArray. this is to close the last infobox before opening a new one
		}
		var ib = new InfoBox(myOptions);
		ib.open(map, this);
    	infoboxArray.push(ib);
	});
}
     
function resetInfobox(){		// close any open infobox before adding new markers to the map 
    if(infoboxArray){
        for(i in infoboxArray){
            infoboxArray[i].close();
        }
        infoboxArray.length = 0;
    }
}

function deleteOverlays() {			// remove all bus stop markers before adding new markers to the map
  if (markersArray) {
    for (i in markersArray) {
      	markersArray[i].setMap(null);
    }
    markersArray.length = 0;
  }
}

function findZoomCenter(lonArray, latArray) {
	var lon_sum = 0, lat_sum = 0;
	var zoomCenter;

	for(var i = 0; i < lonArray.length; i++) {
		lon_sum += lonArray[i];
		lat_sum += latArray[i];
	}

	zoomCenter = new google.maps.LatLng(lat_sum/latArray.length, lon_sum/lonArray.length)
	return zoomCenter;
}


/************ Header ************/

function clearBox(ele) {
  	ele.value = "";
}

function stop() {
	if(confirm("Do you want to stop tracking? The record will not be lost.")) {
		window.location.reload();
	}
}


/************ Search ************/

function go() {
  	var boxValue = document.getElementById("searchBox").value.toLowerCase();
	deleteOverlays();
	lonArray.length = 0;
	latArray.length = 0;

  	d3.csv("data/busStops(soton).csv", function(error, data) {
	  	var dataset = data;
	  	var s_count = 0, c_count = 0;
	  	var streetFlag = 0, commonNameFlag = 0;
		var statusBar = document.getElementById("statusBar");
		OriginOrDestinationFlag = 2;

	  	for (var i = 0; i < dataset.length; i++) {
			var street = dataset[i].Street.toLowerCase();
			
  			if (boxValue.indexOf(street) !== -1) {		
  				statusBar.innerHTML = "We are going to <i>" + dataset[i].Street + "</i><br />" +
  									  "Please pick a bus stop as our destination.";
  				lonArray.push(parseFloat(dataset[i].Longitude));
  				latArray.push(parseFloat(dataset[i].Latitude));
  				titleArray.push(dataset[i].CommonName);
  				streetFlag = 1;
  			} 
  		}

  		if (streetFlag !== 1) {
	  		for (var i = 0; i < dataset.length; i++) {
	  			var commonName = dataset[i].CommonName.toLowerCase();

	  			if (boxValue.indexOf(commonName) !== -1) {		
	  				statusBar.innerHTML = "Your destination is <i>" + dataset[i].CommonName + "</i><br />" +
  									  	  "Please pick a bus stop as our destination.";
	  				lonArray.push(parseFloat(dataset[i].Longitude));
	  				latArray.push(parseFloat(dataset[i].Latitude)); 
  					titleArray.push(dataset[i].CommonName);
	  				commonNameFlag = 1;
	  			}
	  		}
	  	}

  		if ((commonNameFlag === 0) && (streetFlag === 0)) {
  			statusBar.innerHTML = "Can't find it, please try again.";
  		} else {
	  		//add markers and infoboxs for all specified bus stops 
	  		for (var i = 0; i < lonArray.length; i++) {
	  			latlon_s = new google.maps.LatLng(latArray[i], lonArray[i])

	  			var marker_s = addMarker(latlon_s, "images/bus.png");
	  			markersArray.push(marker_s);
	  			addInfobox(marker_s, titleArray[i]);
	  		}

	  		map.setCenter(findZoomCenter(lonArray, latArray));

	  		if (num < 3) {		// use bigger zoom if there's not many markers
	  			map.setZoom(16);
	  		} else if (num >=3 && num < 7) {
	  			map.setZoom(15);
	  		} else if (num >=7) {
	  			map.setZoom(13);
	  		} 

	  		num = 0;			// stoping counting for the id of destination markers
  		}
  	});
}


/************ plot origin & destination flags ************/

function setOrigin(ele) {
	var getnum = ele.id.substring(6);
	var statusBar = document.getElementById("statusBar");

	Origin = [titleArray_o[getnum], lonArray_o[getnum], latArray_o[getnum]];		// Origin: [CommonName, lon, lat]
  	resetInfobox();
	deleteOverlays();

	latlon_o = new google.maps.LatLng(latArray_o[getnum], lonArray_o[getnum])  
	var marker_o = addMarker(latlon_o, "images/origin.png");
	OriDesMarkerArray.push(marker_o);
	polylineArray.push(latlon_o);				// add origin coordinate as the first object to the route array

  	statusBar.innerHTML = "OK! Now search for our destination.";

	// change page header
	var form = document.getElementsByTagName("form");
	var text = document.getElementById("text");
	var refresh = document.getElementById("refresh");
	form[0].removeChild(text);
	form[0].removeChild(refresh);

	var searchBox = document.createElement("input");
	searchBox.setAttribute("type", "text");
	searchBox.setAttribute("id", "searchBox");
	searchBox.setAttribute("value", "Search map");
	searchBox.setAttribute("style", "height:20px");
	searchBox.setAttribute("onfocus", "clearBox(this)");
	form[0].appendChild(searchBox);

	var go = document.createElement("a");
	go.setAttribute("id", "go");
	go.setAttribute("class", "roundButton");
	go.setAttribute("onclick", "go()");
	go.innerHTML = "Go";
	form[0].appendChild(go);
}

function setDestination(ele) {
	var getnum = ele.id.substring(6);
	var statusBar = document.getElementById("statusBar");

	Destination = [titleArray[getnum], lonArray[getnum], latArray[getnum]];		// Destination: [CommonName, lon, lat]
	resetInfobox();
	deleteOverlays();

	latlon_d = new google.maps.LatLng(latArray[getnum], lonArray[getnum])  
	var marker_d = addMarker(latlon_d, "images/destination.png");
	OriDesMarkerArray.push(marker_d);

	statusBar.innerHTML = "We are all set, let's shove off!";
	
	// change page header
	var form = document.getElementsByTagName("form");
	var searchBox = document.getElementById("searchBox");
	var go = document.getElementById("go");
	form[0].removeChild(searchBox);
	form[0].removeChild(go);

	var text = document.createElement("span");
	text.setAttribute("id", "text");
	text.innerHTML = "Tracking....";
	form[0].appendChild(text);

	var stop = document.createElement("a");
	stop.setAttribute("id", "stop");
	stop.setAttribute("class", "squareButton");
	stop.setAttribute("onclick", "stop()");
	stop.innerHTML = "Stop";
	form[0].appendChild(stop);

	// delay for 1s (better UX) before going tracking
	setTimeout("tracking()", 500);
}

function replotOriAndDesFlags() {
	latlon_o = new google.maps.LatLng(Origin[2], Origin[1])  
	latlon_d = new google.maps.LatLng(Destination[2], Destination[1]) 
	var marker_o = addMarker(latlon_o, "images/origin.png");
	var marker_d = addMarker(latlon_d, "images/destination.png");
	OriDesMarkerArray.push(marker_o);
	OriDesMarkerArray.push(marker_d);
}


/************ Tracking ************/

function tracking() {
	isTrackingFlag = 1;
	Gposition = navigator.geolocation.watchPosition(showPosition, showError, {maximumAge: 60000});
}

function recordingBar(showPositionCount) {
	if (showPositionCount === 1) {
		var content = document.getElementById("content");

		var recordingBar = document.createElement("div");
		recordingBar.setAttribute("id", "recordingBar");
		content.insertBefore(recordingBar, content.childNodes[0]);
		
		var bar1 = document.createElement("div");
		bar1.setAttribute("class", "bar");
		recordingBar.appendChild(bar1);
		bar1.innerHTML = "<img src='images/route.png' /> &nbsp; Distance: <span id='bar1_span' class='trackingResults'>" + distance + "</span> mile(s)";

		var bar2 = document.createElement("div");
		bar2.setAttribute("class", "bar");
		recordingBar.appendChild(bar2);
		bar2.innerHTML = "<img src='images/fuel.png' /> &nbsp; Fuel saved: <span id='bar2_span' class='trackingResults'>" + fuelSaved + "</span> gallon(s)";

		var bar3 = document.createElement("div");
		bar3.setAttribute("class", "bar");
		recordingBar.appendChild(bar3);
		bar3.innerHTML = "<img src='images/CO2.png' /> &nbsp; CO2 reduced: <span id='bar3_span' class='trackingResults'>" + CO2Reduced + "</span> kg";
	} else {
		document.getElementById("bar1_span").innerHTML = distance;
		document.getElementById("bar2_span").innerHTML = fuelSaved;
		document.getElementById("bar3_span").innerHTML = CO2Reduced;
	}
}


/************ Compute distance, money, fuel ************/

function computeDistance(point1, point2) {
	var startLatRads = degreesToRadians(point1[1]);
	var startLongRads = degreesToRadians(point1[0]);
	var destLatRads = degreesToRadians(point2[1]);
	var destLongRads = degreesToRadians(point2[0]);

	var Radius = 6371; 			// radius of the Earth in kilometer
	var distance = Math.acos(Math.sin(startLatRads) * Math.sin(destLatRads) + 
				   Math.cos(startLatRads) * Math.cos(destLatRads) *
				   Math.cos(startLongRads - destLongRads)) * Radius ;

	distance = (distance / 1.609344).toFixed(2)		// distance in mile
	return distance;			
}

function degreesToRadians(degrees) {
	radians = (degrees * Math.PI) / 180;
	return radians;
}