
window.onload = init;

var initialisedFlag = 0;

function init() {
	for (var i = 0; i < localStorage.length; i++) {
		if (localStorage.key(i).substring(0, 7) === "ILPT_a_") {
			initialisedFlag = 1;
			break;
		}
	}

	if (!initialisedFlag) {
		// initialise localStorage
		localStorage.setItem("ILPT_a_totalDistance", "0.00");	
		localStorage.setItem("ILPT_a_totalFuelSaved", "0.00");
		localStorage.setItem("ILPT_a_totalCO2Reduced", "0.00");
		localStorage.setItem("ILPT_a_totalMoneySaved", "0.00");

		initialisedFlag = 1;

		for (var i = 0; i < localStorage.length; i++) {
			console.log(localStorage.key(i)+ " : " + localStorage.getItem(localStorage.key(i)));
		}
	} 
	
	// assign the localStorage values to the page
	document.getElementById("section1_body").innerHTML = "<span>" + localStorage.getItem("ILPT_a_totalDistance") + "</span>" + " mile(s)";
	document.getElementById("section2_body").innerHTML = "<span>" + localStorage.getItem("ILPT_a_totalFuelSaved") + "</span>" + " gallon(s)";
	document.getElementById("section3_body").innerHTML = "<span>" + localStorage.getItem("ILPT_a_totalCO2Reduced") + "</span>" + " kg";
	document.getElementById("section4_body").innerHTML = "&pound; " + "<span>" + localStorage.getItem("ILPT_a_totalMoneySaved") + "</span>";

	dataVisualisation();
}

function reset() {
	if (initialisedFlag) {
		localStorage.removeItem("ILPT_a_totalDistance");
		localStorage.removeItem("ILPT_a_totalFuelSaved");
		localStorage.removeItem("ILPT_a_totalCO2Reduced");
		localStorage.removeItem("ILPT_a_totalMoneySaved");

		initialisedFlag = 0;
		init();		// refresh the page
	}
}

function dataVisualisation() {
	var section5_body = document.getElementById("section5_body");
}