
window.onload = init;

function init() {
	var initialisedFlag = 0;
		
	var petrol = document.getElementById("petrol");
	var diesel = document.getElementById("diesel");
	var consumption = document.getElementById("consumption");
	var food = document.getElementById("food");
	var entertainment = document.getElementById("entertainment");
	var car = document.getElementById("car");
	var donation = document.getElementById("donation");

	for (var i = 0; i < localStorage.length; i++) {
		if (localStorage.key(i).substring(0, 7) === "ILPT_s_") {
			initialisedFlag = 1;
			break;
		}
	}

/*	
	// output all localStorage items (for testing) 
	for (var i = 0; i < localStorage.length; i++) {
		console.log(localStorage.key(i)+ " : " + localStorage.getItem(localStorage.key(i)));
	}
*/

	if (!initialisedFlag) {
		// initialise localStorage
		localStorage.setItem("ILPT_s_fuelType", "petrol");	
		localStorage.setItem("ILPT_s_fuelConsumption", "28.8");
		localStorage.setItem("ILPT_s_preference", "food");

		// initialise options in the page
		petrol.setAttribute("checked", "checked");
		consumption.setAttribute("value", "28.8");
		food.setAttribute("checked", "checked");
	} else {
		// get localStorage
		var ls_fuelType = localStorage.getItem("ILPT_s_fuelType");
		var ls_fuelConsumption = localStorage.getItem("ILPT_s_fuelConsumption");
		var ls_preference = localStorage.getItem("ILPT_s_preference");

		// initialise options in the page Settings
		if (ls_fuelType === "petrol") {
			petrol.setAttribute("checked", "checked");
		} else {
			diesel.setAttribute("checked", "checked");
		}

		consumption.setAttribute("value", ls_fuelConsumption);

		switch (ls_preference) {
			case "food": food.setAttribute("checked", "checked"); break;
			case "entertainment": entertainment.setAttribute("checked", "checked"); break;
			case "car": car.setAttribute("checked", "checked"); break;
			case "donation": donation.setAttribute("checked", "checked"); break;
		}
	}
}

function changeFuelType(val) {
	localStorage.setItem("ILPT_s_fuelType", val);	
	saved();	
}

function changeFuelConsumption(val) {
	var consumption = document.getElementById("consumption");

	if (isNaN(val)) {
		alert("'" + val + "'" + " is not a number!");
		consumption.focus();
	} else if (!val) {	
		alert("Please enter a number!");
		consumption.focus();
	} else if (val == 0) {					// the type of val is string, so use "==" instead of "===" 
		alert("No kidding please.");
		consumption.focus();
	} else {
		localStorage.setItem("ILPT_s_fuelConsumption", val);
		saved();	
	}	
}

function changePreference(val) {
	localStorage.setItem("ILPT_s_preference", val);		
	saved();
}

function saved() {
	if (!statusBar) {
		var statusBar = document.createElement("div");
		statusBar.setAttribute("id", "statusBar");

		var content = document.getElementById("content");
		content.insertBefore(statusBar, content.childNodes[0]);
		$("#statusBar").hide().slideDown("fast");
		setTimeout('$("#statusBar").slideUp("fast")', 2000); 
	} 

	statusBar.innerHTML = "Settings saved";
}

function clearBox(ele) {
	ele.value = "";
}